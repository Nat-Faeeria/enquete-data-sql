# Introduction
Activité numérique prévue pour les BTS SIO pour l'initiation au SQL (mais accessible à d'autres niveaux)
On découvre la manipulation des données structurées dans une BDD à l'aide de SQL, pour résoudre une enquête

Jeu testable ici : http://nathanael-gimenez.com/initiation-sql/index.html

# Comment utiliser ce jeu ?
## Composants nécessaires
### Dans tous les cas
 * Java 11 (OpenJDK)
 * Maven
 * Nginx et Let's Encrypt certbot (si vous souhaitez mettre en place un chiffrement TLS)

### Installation manuelle
 * Mysql ou MariaDB
 * Un ou deux serveurs (pour le serveur web et le SGBD). Peut facilement être lancé depuis un cloud.

### Installation avec Docker
 * Docker
 * Toujours et encore, des serveurs ou un cloud. 

## Les étapes
### Installation manuelle
 1. Téléchargez les sources
 2. Dans les sources, modifiez le fichier .env.example en y indiquant les bonnes valeurs, puis renommez le .env
 3. À la racine, lancez la commande "mvn clean package". Vous obtenez un fichier grizzly-service-0.1-SNAPSHOT-jar-with-dependencies.jar
(4. Bonus esthétique (commandes bash) : mv grizzly-service-0.1-SNAPSHOT-jar-with-dependencies grizzly.jar)
 5. Lancez la commande "java -jar grizzly.jar". Vous devriez voir le serveur se lancer
 6. Installez Mysql ou MariaDB sur une machine accessible par le serveur web (ça peut être la même)
 7. Lancez le SGBD en root
 8. Créez une base de données (CREATE DATABASE edsnt) et un utilisateur à droits limités (CREATE USER 'user'@'%' IDENTIFIED BY 'mot de passe solide' puis GRANT SELECT ON edsnt.* TO 'user'@'%')
 9. Testez le tout (vérification des données en base, puis connexion à l'adresse IP ou domaine de la machine où vous avez déployé les applicatifs, port 80) et profitez !

### Installation avec Docker
 1. Téléchargez les sources
 2. Dans les sources, modifiez le fichier .env.example en y indiquant les bonnes valeurs, puis renommez le .env
 3. A la racine, lancez la commande 'mvn clean package'. Vous obtenez un fichier grizzly-service-0.1-SNAPSHOT-jar-with-dependencies.jar
 4. (Obligatoire !) Renommez le jar en grizzly.jar
 5. Rendez le script runWithDocker.sh exécutable (sudo chmod u+x runWithDocker.sh)
 6. Lancez le script. Vous pouvez lui passer deux paramètres : $1 -> la localisation du .jar et du .sql, $2 -> le mot de passe root de Mysql. Par défaut, les valeurs seront $(pwd) et BadPassword.
 7. Vérifiez que les deux conteneurs eds-db et eds-web (avec un docker ps)
 8. Connectez-vous à l'adresse IP ou domaine de la machine où vous avez déployé les conteneurs. Le port par défaut est 80.
 
 ### Compatibilité HTTPS
 Si vous souhaitez rendre votre site accessible par HTTPS, il faut modifier le code de la classe MainLauncher pour changer le port d'écoute de Grizzly vers 5000 (ou autre).
 
 Ensuite, utilisez Let's Encrypt certbot pour générer des certificats si vous n'en avez pas.
 
 Configurez un server Nginx en reverse proxy vers l'application web sur le port 5000. Configurez ce serveur pour qu'il écoute sur les ports 80 et 443, et pour qu'il connaisse vos certificats.
 
 Testez.

# Licence
Base de code du projet issue des ressources en ligne de [Mickael Baron](https://mickael-baron.fr/apropos/), sur Jersey avec Grizzly
Activité d'origine issue du site de [Marie Duflot-Kremer](https://members.loria.fr/MDuflot/) (que je remerci au passage pour cette activité ainsi que ses nombreuses autres activités d'informatique déconnectée)

Ce projet est à mon unique initiative.

Ce projet est sous licence [Creative Commons Attribution - Non Commercial - Share Alike](https://creativecommons.org/licenses/by-nc-sa/4.0/)
