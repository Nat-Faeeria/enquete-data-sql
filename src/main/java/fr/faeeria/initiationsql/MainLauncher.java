package fr.faeeria.initiationsql;

import java.net.URI;
import java.util.logging.Level;

import javax.ws.rs.core.UriBuilder;

import io.github.cdimascio.dotenv.Dotenv;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;


public class MainLauncher {
	
	public static final URI BASE_URI = getBaseURI();

    private static URI getBaseURI() {
        Dotenv dotenv = Dotenv.configure().directory("/").load();
        return UriBuilder.fromUri(dotenv.get("WEB_URL")).port(Integer.parseInt(dotenv.get("WEB_PORT"))).build();
    }

    public static void main(String[] args) {
        ResourceConfig rc = new ResourceConfig();
        rc.registerClasses(App.class, Resource.class);
        rc.register(new CorsFilter());
        rc.property(LoggingFeature.LOGGING_FEATURE_LOGGER_LEVEL_SERVER, Level.WARNING.getName());

        try {
            HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, rc);
            server.start();

            System.out.println(String.format(
                    "Jersey app started at %s",
                    BASE_URI, BASE_URI));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
