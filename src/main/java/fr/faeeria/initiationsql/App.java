package fr.faeeria.initiationsql;

import io.github.cdimascio.dotenv.Dotenv;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.sql.*;

@Path("/initiation-sql")
public class App {

	@GET
	@Path("/{page}")
	@Produces(MediaType.TEXT_HTML)
	public Response getPage(@PathParam("page") String page) {
		try {
		 InputStream stream = getClass().getClassLoader().getResourceAsStream(page);
		 if (stream.equals(null)) {
             return Response.status(Response.Status.NOT_FOUND).entity("<h1>404 - Page Not Found, Sorry ! </h1>").build();
         }
		 return Response.ok(stream).build();
		} catch (Exception e) {
			e.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).entity("<h1>Page Not Found, Sorry ! </h1>").build();
        }
	}

	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public Response sendDBRequest(String rawRequest) {
		String result = new String();

		String requestSQL = rawRequest;

		Dotenv dotenv = Dotenv.configure().directory("/").load();;
		StringBuilder dbUrlBuilder = new StringBuilder();
		dbUrlBuilder.append("jdbc:mysql://")
				.append(dotenv.get("DB_ADDRESS"))
				.append(":").append(dotenv.get("DB_PORT"))
				.append("/").append(dotenv.get("DB_NAME"))
				.append("?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
		String dbUrl = dbUrlBuilder.toString();
		try {
			Connection con = DriverManager.getConnection(
                    dbUrl, dotenv.get("DB_USER"),dotenv.get("DB_PASSWORD"));

			Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs=stmt.executeQuery(requestSQL);
			int nbColumns = rs.getMetaData().getColumnCount();
			String comma = " , ", retChar = "\n";
			StringBuilder sb = new StringBuilder();
			while (rs.next()) {
				for (int i=1;i<nbColumns;i++) {
					Object o = rs.getObject(i);
					sb.append(getStringFromRS(o));
					sb.append(comma);
				}
				Object o = rs.getObject(nbColumns);
				sb.append(getStringFromRS(o));
				sb.append(retChar);
			}
			result = sb.toString();
			if (result == "") {
				result = "La requête n'a pas renvoyé de résultat !";
			}
		} catch (SQLException e) {
			result = e.getMessage();
		}
		return Response.ok(result).build();
	}

	private String getStringFromRS(Object o) {
		if (o == null) {
			return "NULL";
		} else {
			return o.toString();
		}
	}
}
