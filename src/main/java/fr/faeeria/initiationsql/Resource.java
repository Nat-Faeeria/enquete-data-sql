package fr.faeeria.initiationsql;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.io.InputStream;

@Path("/initiation-sql/resources/{dir}/{file}")
public class Resource {

    @GET
    public Response getResourceFile(@PathParam("dir") String dir, @PathParam("file") String file){
        System.out.println(dir+"/"+file);
        try {
            InputStream stream = getClass().getClassLoader().getResourceAsStream(dir+"/"+file);
            if (stream.equals(null)) {
                return Response.status(Response.Status.NOT_FOUND).entity("<h1> 404 - Resource not Found, Sorry !</h1>").build();
            }
            return Response.ok(stream).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).entity("<h1>Page Not Found, Sorry ! </h1>").build();
        }
    }
}
