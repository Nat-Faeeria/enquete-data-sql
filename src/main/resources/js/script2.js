const contenu = {
    page1: { title: "Début de l'enquête",
        content: "Récupère la liste des étudiants (numéro, nom et prénom) de l'université. \
                  Trie cette liste par ordre alphabétique pour la rendre plus lisible ! "
    },
    page2: { title: 'Indice 1 : Muy logo',
        content: "En inspectant l'arrière plan de la photo, on peut repérer un diplôme de \
                  l'université. Le logo sur ce diplôme est le nouveau logo, apparu en 2012. \
                  On peut donc réduire l'ensemble des suspects aux étudiants diplômés après \
                  2012.<br/><br/>\
                  Modifie ta requête avec cette nouvelle donnée pour obtenir la nouvelle liste \
                  des suspects. (Tu vas avoir besoin d'une jointure ! )<br/><br/>\
                  Tu vas avoir besoin d'utiliser la fonction <b>year()</b>. Quand un des champs de la \
                  base de données contient une date complète, la fonction <b>year()</b> permet de ne renvoyer \
                  que l'année.<br/>\
                  Ex. : <b>year(date_d)</b>"
    },
    page3: { title: 'Indice 2 : Comme-ci comme-ça',
        content: "La filière du diplôme est un peu cachée, mais on voit qu'elle commence par \
                 les lettres 'Sc'.<br/>\
                 Tu peux utiliser l'opérateur LIKE pour trouver toutes les valeurs ressemblant \
                 à une autres.<br/>\
                 Par exemple, écrire <b>WHERE prenom LIKE 'Ph%</b> permet de récupérer \
                 tous les prénoms qui commencent par 'Ph', tandis que <b>WHERE prenom LIKE '%ine'</b> \
                 permet de récupérer tous les prénoms se finissant par 'ine'.<br/><br/> \
                 Modifie ta requête précédente pour trouver réduire la liste des étudiants suspects \
                 en ne prenant en compte que ceux concernés par la mystérieuse filière en 'Sc'.<br/><br/>\
                 Tu peux ajouter des conditions dans <b>WHERE</b> à l'aide de l'opérateur <b>AND</b><br/>\
                 Exemple : <b>WHERE prenom='machin' AND nom='chose'</b>"
    },
    page4: { title: 'Indice 3 : De suite les grands mots !',
        content: "Le mail contient une autre phrase : ''La physique, c'est nul !''. \
                  Le coupable ne fait donc probablement pas partie d'une filière comportant \
                  des cours de physique.<br/><br/>\
                  Trouve un moyen de ne sélectionner que les étudiants qui n'ont pas de cours\
                  de physique en modifiant la requête précédante.<br/><br/>\
                  Indices : \
                  <ol> \
                  <li>N'hésite pas à commencer par lister le contenu de certaines tables qui peuvent \
                  t'intéresser</li><br/>\
                  <li>Tu peux utiliser l'opérateur <b>!=</b> pour vérifier que deux valeurs ne sont pas égales l'une à l'autre.<br/>\
                  Ex.: <b>nom != 'Jean'</b></li><br/>\
                  <li>Ici on va avoir un petit souci : on veut exclure totalement une filière en fonction d'une seule des\
                  matières de cette filière. Pour s'en sortir, il va nous falloir une requête imbriquée : \
                    <ul>\
                        <li>On commence par écrire la requête pour trouver le code de la/des filières donnant des cours de physique</li>\
                        <li>On prend notre requête de base, et dans le WHERE on rajoute une condition 'Filieres.code NOT IN ()'</li>\
                        <li>On place la requête trouvent le/les code(s) de filières entre les parenthèses</li>\
                    </ul>\
                    Cela nous permet d'exclure (ou inclure avec un simple IN ) un ou des enregistrements en fonction de \
                    valeurs retournées par une autre requête.\
                    </li>"
    },
    page5: { title: 'Indice 4 : Dial M for Murder',
        content: "Le mail est signé M.P. . Trouve la liste des étudiants dont les initiales sont \
                  M.P. en modifiant la requête précédente."
    },
    page6: { title: 'Indice 5 : Fais pas genre !',
        content: "La photo compromettante a été diffusée dans un mail, qui contenait la phrase : \
                  ''La façon dont j'ai été traité à la visite médicale m'a choqué ! '' <br/><br/>\
                  Quelle information peut-on en déduire sur l'auteur ?<br/>\
                  Ecris une requête donnant la liste triée des étudiants suspectés.<br/><br/>\
                  N'oublie pas de mettre des guillemets autour des valeur textuelles ! "
    },
    page7: { title: "Indice 6 : Des noms d'oiseaux",
        content: "Le post-scriptum contient deux citations : ''Le trop de confiance attire le \
                  danger.'' et ''Aux âmes bien nées, la valeur n'attends point le nombre des \
                  années.''<br/><br/>\
                  Grâce à ces citations, retrouve le livre que l'étudiant a pu emprunter (demande à Google ;) )\
                  , puis modifie ta requête pour prendre en compte cette information.<br/><br/>"
    },
    page8: { title: 'La fin ...',
        content: "Normalement, tu viens de trouver le coupable grâce à ta requête ! Félicitations !<br/>\
                  Si ce n'est pas le cas, reviens en arrière et essaie de faire plusieurs \
                  requêtes séparées, puis de recouper les données petit à petit.<br/>\
                  N'oublie pas de bien sauvegarder ta requête dans un fichier à part pour \
                  la correction !<br/<>\
                  Au cours de ce jeu, tu as utilisé le langage SQL !</b> <br/>\
                  Le SQL est utilisé dans toutes les bases de données relationnelles, et dans la majeure partie des \
                  programmes informatiques utilisant une BDD relationnelle (et des sites webs, du coup)<br/>\
                  La base utilisée n'est pas la mieux conçue au monde : il y a quelques grosses erreurs de conception.\
                  Essaie de les trouver !"
    }

}

function fillInstructionsTemplate(pageNumber) {
    const destination = document.getElementById("text-to-change");
    document.querySelector("#output").value = "";
    destination.innerHTML = "";
    const fragment = document.getElementById("instructions-template");
    const instance = document.importNode(fragment.content, true);
    const content = contenu["page"+pageNumber];
    instance.querySelector('#page-number').setAttribute("pageNumber", pageNumber);
    instance.querySelector("#instructions-title").innerHTML = content.title;
    instance.querySelector("#instructions-content").innerHTML = content.content;
    destination.appendChild(instance);
    document.querySelector('body').scrollIntoView();
}

function sendRequest() {
    const content = document.querySelector('#input').value;
    document.querySelector("#output").value = "";
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/initiation-sql/');
    xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
            fillOutput(xhr.response);
        } else {
            console.log(xhr.statusText);
            fillOutput("Il y a eu une erreur, veuillez réessayer !")
        }
    };
    xhr.onerror = () => console.log(xhr.statusText);
    xhr.send(content);
}

function fillOutput(value) {
    document.querySelector("#output").value = value;
}

fillInstructionsTemplate(1)

function nextQuestion() {
    const pageNumber = parseInt(document.getElementById("page-number").getAttribute("pageNumber"), 10)+1;
    if (pageNumber==8){
        document.querySelector("#last-button").style.display="none";
        fillInstructionsTemplate(pageNumber);
    } else if (pageNumber < 9) {
        fillInstructionsTemplate(pageNumber);
    }
}

function previousQuestion() {
    const pageNumber = parseInt(document.getElementById("page-number").getAttribute("pageNumber"), 10) - 1;
    if (pageNumber > 0) {
        fillInstructionsTemplate(pageNumber);
        document.querySelector("#last-button").style.display="inline-block";
    } else {
        window.location.href = "page1.html";
    }
}
